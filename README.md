# WobbleTron #

### Monophonic software synth with note generation powered by cellular automata. Built using pure data https://puredata.info/ ###
_________________________________________________________________________________________________________________________

### Cellular Automata

Inspired by Shiffman's (2012, chapter 7) book "The Nature of Code" (http://natureofcode.com/book/chapter-7-cellular-automata/), he describes CA (Cellular Automata) as a model of a system of "cell" objects that live on a grid, each has a state – on or off – and that each cell has a neighbourhood of other cells.
  
He goes on to explain that the development of CA systems originated from researchers Stanislaw Ulam and John von Neumann, who were studying crystal growth at Los Alamos National Laboratory in New Mexico in the 1940's.  
However, Shiffman (2012, chapter 7) notes that the most significant scientific work surrounding CA was that of Stephen Wolfram in 2002. Wolfram's book "A New Kind of Science" (http://www.wolframscience.com/nksonline/toc.html) discusses how CA is relevant to many studies from biology, chemistry and physics.

The WobbleTron uses a form of CA based upon the ideas of John Conway's "Game of Life" (https://www.youtube.com/watch?v=FdMzngWchDk) and conforms it to an 8x8 grid.

Note data is mapped from a column of 8 cells called a beatColumn. The amount of live (on) cells in a beatColumn determines the velocity of the note played (0 - 127) and the note is determined by the most popular (most neighbours alive immediately surrounding) cell. Low notes are produced from cells at the bottom of the grid and high notes from the top. All notes are mapped to a key and scale set on the synth interface.

____
### Prerequisites

In order to run the WobbleTron you will need to have pure data extended installed on your system. Pure Data is free and available here https://puredata.info/downloads/pd-extended follow the guides online on how to set Pd up on your OS.

Once installed make sure you have all the files downloaded including the folder called CA. Make sure this is in the same folder as the WobbleTron.pd patch as shown below.

![Screen Shot 2015-05-07 at 17.32.40.png](https://bitbucket.org/repo/jyzG9E/images/2844073336-Screen%20Shot%202015-05-07%20at%2017.32.40.png)

### Interface
Open WobbleTron.pd

There are two main components to the WobbleTron, the Cellular Automata grid and the synth. 

![Screen Shot 2015-05-07 at 17.28.35.png](https://bitbucket.org/repo/jyzG9E/images/2073376920-Screen%20Shot%202015-05-07%20at%2017.28.35.png)

The WobbleTron uses a hybrid of wavetable and additive synthesis for tone generation and has several controls for modulating the sound.

### Quick start
Each module is explained in detail below and it is recommended that you read how these parts work to gain a greater understanding of how to play the synth. However, the WobbleTron can produce sound with only a few clicks. To begin making music with the WobbleTron toggle the PWR on and move the VOL slider to the right to increase output.

![Screen Shot 2015-05-07 at 19.52.33.png](https://bitbucket.org/repo/jyzG9E/images/2690417248-Screen%20Shot%202015-05-07%20at%2019.52.33.png) 

*NOTE: make sure the output of your sound card is selected in Pd's settings under media > audio settings and that your volume is at a safe setting before turing the WobbleTron on.* 

To begin note generation toggle the MelodyGen on. ![Screen Shot 2015-05-07 at 19.52.38.png](https://bitbucket.org/repo/jyzG9E/images/2732714602-Screen%20Shot%202015-05-07%20at%2019.52.38.png)

You will see the LED's above the CA grid move with each beat (An 8th note or quaver)

You can now begin to adjust other parameters to modulate the sound. Happy Wobbling!
____
#Synth#
========
### PWR & VOL

![Screen Shot 2015-05-07 at 17.55.49.png](https://bitbucket.org/repo/jyzG9E/images/2819772091-Screen%20Shot%202015-05-07%20at%2017.55.49.png)

The PWR toggle button enables DSP within pure data if currently disabled and enables the WobbleTron to output sound. *NOTE: If PWR is deselected Pd's DSP remains active but no sound will be produced.* VOL controls the volume of the output.

### MelodyGen, Tempo, Key & Scale

![Screen Shot 2015-05-07 at 18.28.47.png](https://bitbucket.org/repo/jyzG9E/images/1164346300-Screen%20Shot%202015-05-07%20at%2018.28.47.png)

The MelodyGen toggle activates the note generation module and begins to extract note data from the CA grid, this is shown with blinking LED's above each column of 8 cells. *NOTE: if the CA grid is toggled off (see Cellular Automata grid below), the grid will behave like an 8 step sequencer.* The tempo of the notes generated can be controlled via the knob on the far right (Farage?) and the current tempo in BPM will be displayed.

The key and scale of the generated notes can be changed using the up and down buttons next to the corresponding heading. The current key and scale are displayed accordingly. *NOTE: There are only four scales currently implemented, these are; Major, Natural Minor, Harmonic Minor and Lydian.*

### Octave

![Screen Shot 2015-05-07 at 18.41.43.png](https://bitbucket.org/repo/jyzG9E/images/1745501133-Screen%20Shot%202015-05-07%20at%2018.41.43.png)

The octave buttons change the relative octave of the notes being produced. The current octave is displayed in the centre.

### Blend & Glide

![Screen Shot 2015-05-07 at 18.27.35.png](https://bitbucket.org/repo/jyzG9E/images/3346310490-Screen%20Shot%202015-05-07%20at%2018.27.35.png)

The blend slider bar changes the tone generation from sine wave (SIN) to square wave (SQR) or any variant in between. The glide slider bar changes the duration (0 - 250 ms) of the glissando between notes.

### Attack & Decay

![Screen Shot 2015-05-07 at 18.35.56.png](https://bitbucket.org/repo/jyzG9E/images/348821767-Screen%20Shot%202015-05-07%20at%2018.35.56.png)

The attack and decay sliders control the attack (25ms - 250ms) and decay (250ms - 2500ms) of the generated notes.

### Here comes the Fuzz!

![Screen Shot 2015-05-07 at 18.40.45.png](https://bitbucket.org/repo/jyzG9E/images/2742689282-Screen%20Shot%202015-05-07%20at%2018.40.45.png)

The FUZZ is a clipping distortion effect that has independent left and right controls for the left and right outputs. The blinking LED below each dial indicates when the signal is being clipped for each channel.

### Wobble Knobs

![Screen Shot 2015-05-07 at 19.23.24.png](https://bitbucket.org/repo/jyzG9E/images/2484680730-Screen%20Shot%202015-05-07%20at%2019.23.24.png)

The wobble knobs are the WobbleTron's namesake. Each dial adjusts the frequency – by up to 0.75 Hz – of each tone generators root note and then sums the signal. This causes frequency beating and gives the WobbleTron its distinct sound.
____
#Cellular Automata Grid#
===============================

The CA grid has several components for drawing patterns, enabling life, altering cell behaviour, saving and loading preset patterns and reseting. Each element will be explained below.

### Drawing Cells
![Screen Shot 2015-05-07 at 19.43.51.png](https://bitbucket.org/repo/jyzG9E/images/2323833467-Screen%20Shot%202015-05-07%20at%2019.43.51.png)

The WobbleTron loads a default pattern into the grid for a quick way to play sounds. Each cell can be toggled on or off to create different patterns thus changing the notes played.

### Presets
![Screen Shot 2015-05-11 at 18.36.17.png](https://bitbucket.org/repo/jyzG9E/images/3441717066-Screen%20Shot%202015-05-11%20at%2018.36.17.png)

You can save and load your own patterns to the grid using the preset buttons. To create a preset, scroll the number box to select a preset slot, draw a new pattern on the grid (or use one created by the CA) and click save, the preset pattern will now be stored in that slot. To recall a saved preset, scroll the number box to the desired preset slot and click load.

### CA TGL, RESET & Ruleset
![Screen Shot 2015-05-11 at 18.52.25.png](https://bitbucket.org/repo/jyzG9E/images/964728722-Screen%20Shot%202015-05-11%20at%2018.52.25.png)

The CA TGL, reset and ruleset buttons control how the CA works. CA TGL turns the "game of life" on, every eighth beat the grid will be refreshed with the new cell states until CA TGL is turned off. 

Reset, does what it says on the tin. *NOTE: reset will also clear any drawn in patterns even if CA TGL is turned off* 

Ruleset allows the user to change the variables for life and death, this can result in the grid becoming very populated of very sparse depending upon the chosen ruleset.

___

These instructions should give you enough information to begin crafting sounds with the WobbleTron.

Any questions email info@daudiomusicgroup.com

Happy Wobbling!